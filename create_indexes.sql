USE monolith;
SELECT 'Creating monolith indexes...' AS ``;

# DROP INDEX users_index ON users;
CREATE INDEX users_index ON users(username, password);
# DROP INDEX objects_index ON objects_and_values;
CREATE INDEX objects_index ON objects_and_values(object);

SELECT 'Success!' AS ``;
SELECT '**********************************************' AS ``;


SELECT 'Creating microservices authorization indexes...' AS ``;

USE authorization;
# DROP INDEX users_index ON users;
CREATE INDEX users_index ON users(username, password);

SELECT 'Success!' AS ``;
SELECT '**********************************************' AS ``;

USE objects;
SELECT 'Creating microservices objects indexes...' AS ``;

# DROP INDEX objects_index ON objects_and_values;
CREATE INDEX objects_index ON objects_and_values(object);

SELECT 'Success!' AS ``;
SELECT '**********************************************' AS ``;
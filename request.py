import os

import requests
import time

script_dir = os.path.dirname(__file__)

MONOLITH_PATH = script_dir + 'monolith/scenario.txt'
MONOLITH_PORT = '5000'

MICROSERVICES_PATH = script_dir + 'microservices/scenario.txt'
MICROSERVICES_PORT = '6000'


def make_requests(path, port):
    scenario = open(path, 'r')

    start = time.time()
    requests_count = 0

    for line in scenario:
        requests_count += 1
        params = line.split(' ')

        params[3] = params[3][:-1]

        r = requests.post(
            "http://127.0.0.1:%s/object/get/" % port,
            json={'username': params[0], 'password': params[1], 'object': params[2]}
        )

        print(r.status_code, r.reason, r.content)

    end = time.time()
    print("\nElapsed time:")
    print(end - start)

    print("\nRPS: " + str(requests_count / (end - start)))


code = input("Type 1 to make requests for monolith, or 2 for microservices:\n")
try:
    code = int(code)
    if int(code) == 1:
        make_requests(MONOLITH_PATH, MONOLITH_PORT)
    elif int(code) == 2:
        make_requests(MICROSERVICES_PATH, MICROSERVICES_PORT)
    else:
        print("Wrong code!")
except Exception as e:
    print(e)

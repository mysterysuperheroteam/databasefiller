CREATE DATABASE IF NOT EXISTS authorization;

USE authorization;

# процедура по выводу текстовых сообщений
DELIMITER ;;

DROP PROCEDURE IF EXISTS printf;
CREATE PROCEDURE printf(thetext TEXT)
  BEGIN

    SELECT thetext AS ``;

  END;

;;

DELIMITER ;

CALL printf('Database created');

CALL printf('Creating users table...');
CREATE TABLE IF NOT EXISTS users (
  id       INT NOT NULL AUTO_INCREMENT,
  username VARCHAR(255),
  password VARCHAR(255),
  PRIMARY KEY (id)
)
  ENGINE = innodb;
CALL printf('Users table created...');

CALL printf('Success!');

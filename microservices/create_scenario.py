import os

import MySQLdb

# expected that users_id_max equals users_count in fill_database.py
users_id_max = 10000
# expected that objects_id_max equals objects_count in fill_database.py
objects_id_max = 10000


def create_scenario():
    # getting users from authorization.users
    conn = MySQLdb.connect(host="localhost", user="rw", passwd="123", db="authorization")
    cursor = conn.cursor()
    cursor.execute("SELECT username, password FROM users;")
    users = cursor.fetchall()
    cursor.close()
    conn.close()

    # getting objects from objects.objects_and_values
    conn = MySQLdb.connect(host="localhost", user="rw", passwd="123", db="objects")
    cursor = conn.cursor()
    cursor.execute("SELECT object, value FROM objects_and_values;")
    objects_and_values = cursor.fetchall()
    cursor.close()
    conn.close()

    scenario = open(os.path.dirname(__file__) + '/scenario.txt', 'w')

    for i in range(0, users_id_max):
        # if i % 100 == 0:
        scenario.write(
            str(users[i][0]) + " " + str(users[i][1]) + " " +
            str(objects_and_values[i][0]) + " " + str(objects_and_values[i][1]) + "\n"
        )


create_scenario()

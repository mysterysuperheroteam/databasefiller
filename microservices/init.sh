#!/usr/bin/env bash
echo "Warning! This script will drop authorization and objects databases and refill all tables!"
echo "Continue? Print Y if yes:"
read answer

if [ "$answer" = "Y" ]
then
    echo "Dropping authorization and objects databases:"
    mysql < drop_databases.sql -u rw -p123
    echo -e "*******************************************************\n"

    echo "Creating authorization database schema:"
    mysql < authorization_schema.sql -u rw -p123
    echo -e "*******************************************************\n"


    echo "Creating objects database schema:"
    mysql < objects_schema.sql -u rw -p123
    echo -e "*******************************************************\n"

    echo "Executing fill_database.py"
    python3 fill_databases.py
    echo -e "*******************************************************\n"

    echo "Executing create_scenario.py"
    python3 create_scenario.py
    echo -e "*******************************************************\n"

    echo "End."
fi
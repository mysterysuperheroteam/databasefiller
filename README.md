# Databasefiller
Предполгается, что на машине, на которой будут запускаться скрипты, содержащиеся в данном репозитории, установлена Unix-подобная операционная система, Python 3.6+, СУБД MySQL, работает демон mysqld.

### Для чего нужен репозиторий:
1. Создать БД монолитного и микросервисного приложений.
2. Наполнить созданные БД тестовыми данными.
3. Проиндексировать созданные БД.
4. Создать сценарии тестирования.
5. Получить RPS.

### Последовательность действий:
1. Убедиться, что на тестовой машине установлено все необходимое, описанное в начале файла.
2. Загрузить репозиторий:
```sh
$ cd path_to_your_folder
$ git clone https://MysterySuperhero@bitbucket.org/mysterysuperheroteam/databasefiller.git
```
3. Перейти в папку проекта:
```sh
$ cd databasefiller
```
4. Зайти в СУБД MySQL root-пользователем или каким-нибудь другим, имеющим права на создание пользователей, и выполнить код из **create_user.sql**. Будет создан пользователь "rw" с паролем "123".
5. Создать и наполнить БД для монолитного сервиса:
```sh
$ cd monolith
$ chmod +x init.sh # выдать права на исполенние скрипту init.sh
$ ./init.sh # запустить скрипт
```
6. База микросервисного приложения наполняется аналогично пункту 5.
7. Проиндексировать БД монолита и микросервисов, выполнив команду:
```sh
$ mysql < create_indexes.sql -u rw -p123
```
8. Запустить монолит / микросервисы.
9. Нагрузить запустив файл **request.py**
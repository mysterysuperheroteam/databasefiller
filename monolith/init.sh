#!/usr/bin/env bash
echo "Warning! This script will drop monolith database and refill all tables!"
echo "Continue? Print Y if yes:"
read answer

if [ "$answer" = "Y" ]
then
    echo "Dropping database:"
    mysql < drop_database.sql -u rw -p123

    echo "Creating database schema:"
    mysql < monolith_schema.sql -u rw -p123

    echo "Executing fill_database.py"
    python3 fill_database.py

    echo "Executing create_scenario.py"
    python3 create_scenario.py
fi
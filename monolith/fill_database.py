import string

import MySQLdb
import names
import random

users_count = 1000000
objects_count = 1000000


def get_username():
    return names.get_last_name() + str(random.randrange(100000))


def get_password():
    return str(random.randrange(100000))


def get_random_string(N):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N))


def fill_users_table(conn, cursor):
    print("Insert %d rows to users table" % users_count)

    cursor.execute("DELETE FROM users WHERE 1 = 1")

    insert_statement = (
        "INSERT INTO users (username, password) VALUES (%(username)s, %(password)s)"
    )

    for i in range(0, users_count):
        username = get_random_string(10)
        # username = get_username()
        password = get_random_string(10)
        # password = get_password()
        insert_data = {
            'username': username,
            'password': password
        }
        cursor.execute(insert_statement, insert_data)

        if i % 100 == 0 and i != 0:
            print("%d rows inserted" % i)

    conn.commit()
    print("Table \"users\" filled.")


def fill_objects_table(conn, cursor):
    print("Insert %d rows to objects_and_values table" % objects_count)

    cursor.execute("DELETE FROM objects_and_values WHERE 1 = 1")
    insert_statement = (
        "INSERT INTO objects_and_values (object, value) VALUES (%(object)s, %(value)s)"
    )

    for i in range(0, users_count):
        rand_object = get_random_string(3)
        rand_value = get_random_string(5)
        insert_data = {
            'object': rand_object,
            'value': rand_value
        }
        cursor.execute(insert_statement, insert_data)

        if i % 100 == 0 and i != 0:
            print("%d rows inserted" % i)

    conn.commit()
    print("Table \"objects_and_values\" filled.")


def fill_database():
    print("Database filling started...")
    conn = MySQLdb.connect(host="localhost", user="rw", passwd="123", db="monolith")
    cursor = conn.cursor()

    fill_users_table(conn, cursor)
    fill_objects_table(conn, cursor)

    cursor.close()
    conn.close()
    print("Success!")


fill_database()

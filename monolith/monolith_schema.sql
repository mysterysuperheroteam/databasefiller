CREATE DATABASE IF NOT EXISTS monolith;

USE monolith;

# процедура по выводу текстовых сообщений
DELIMITER ;;

DROP PROCEDURE IF EXISTS printf;
CREATE PROCEDURE printf(thetext TEXT)
  BEGIN

    SELECT thetext AS ``;

  END;

;;

DELIMITER ;

CALL printf('Database created');

CALL printf('Creating users table...');
CREATE TABLE IF NOT EXISTS users (
  id       INT NOT NULL AUTO_INCREMENT,
  username VARCHAR(255),
  password VARCHAR(255),
  PRIMARY KEY (id)
)
  ENGINE = innodb;
CALL printf('Users table created...');

CALL printf('Creating object-value table...');
CREATE TABLE IF NOT EXISTS objects_and_values (
  id     INT NOT NULL AUTO_INCREMENT,
  object VARCHAR(255),
  value  VARCHAR(255),
  PRIMARY KEY (id)
)
  ENGINE = innodb;
CALL printf('Object-value table created...');

CALL printf('Success!');
